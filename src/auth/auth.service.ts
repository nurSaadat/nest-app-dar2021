import { BadRequestException, Injectable, UnauthorizedException } from "@nestjs/common";
import { LoginRequest, LoginResponse, RegistrationRequest } from "src/shared/types";

const users = [
    {
        username: 'nursaadat',
        password: '123',
        token: '1234',
        firstName: 'Saadat',
        lastName: 'Nursultan',
        avatar: 'https://moodle.nu.edu.kz/pluginfile.php/5354/user/icon/boost/f1?rev=2407422'

    }
];

let token = 1;

@Injectable()
export class AuthService {
    login(data: LoginRequest): LoginResponse {
        if (!data.password || !data.username) {
            throw new BadRequestException();
        }

        const user = users
            .find(u => u.username === data.username && u.password === data.password);

        if (user) {
            return {
                token: user.token
            }
        }
        throw new UnauthorizedException();
    }

    getProfile(token: string) {
        const user = users.find(u => u.token === token)

        if (user) {
            return {
                ...user,
                password: ''
            }
        }
        throw new UnauthorizedException();
    }

    register(data: RegistrationRequest): LoginResponse {
        // console.log(data);

        if (!data.password || !data.username || !data.firstName || !data.lastName) {
            throw new BadRequestException();
        }

        let currentToken = token.toString();
        token += 1;
 
        users.push(
            {
                username: data.username,
                password: data.password,
                token: currentToken,
                firstName: data.firstName,
                lastName: data.lastName,
                avatar: 'https://www.drone-io.com/wp-content/uploads/2018/11/profile-icon-9.png'
            }
        );

        return {
            token: currentToken
        }
    }

}