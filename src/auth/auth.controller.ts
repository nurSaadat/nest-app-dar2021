import { Body, Controller, Get, Post, Headers } from "@nestjs/common";
import { LoginRequest, RegistrationRequest } from "src/shared/types";
import { AuthService } from "./auth.service";

@Controller('auth')
export class AuthController {

    constructor(private readonly authService: AuthService) {}

    @Post('login')
    postLogin(@Body() data: LoginRequest) {
        return this.authService.login(data);
    }

    @Post('register')
    postRegister(@Body() data: RegistrationRequest) {
        console.log(data);
        return this.authService.register(data);
    }

    @Get('profile')
    getProfile(@Headers('Authorization') token: string) {
        return this.authService.getProfile(token);
    }
}